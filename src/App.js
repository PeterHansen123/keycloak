import "./App.css";
import { TokenPage } from "./components/TokenPage";
import { AdminPage } from "./components/AdminPage";
import { HomePage } from "./components/HomePage";
import { BrowserRouter, Routes, Route, Link } from "react-router-dom";
import keycloak from "./keycloak/keycloak";
import { LoggedinRoute } from "./hoc/LoggedInRoute";
import { RoleCheckRoute } from "./hoc/RoleCheckRoute";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <h1>Hello world</h1>
        <nav>
          <li><Link to = "/">Home</Link></li>
          <li><Link to = "/token">Token</Link></li>
          <li><Link to = "/admin">Admin</Link></li>
          <li><button onClick={ () => keycloak.login() }>Login</button></li>
          <li><button onClick={ () => keycloak.logout() }>Logout</button></li>
          <li><button onClick={ () => {console.log(keycloak.authenticated)} }>debug</button></li>
          {keycloak.authenticated && <li>Hello {keycloak.tokenParsed.name}</li>}
        </nav>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/token" element={
            <LoggedinRoute>
                <TokenPage />
            </LoggedinRoute>
         } />
          <Route path="/admin" element={
          <RoleCheckRoute role="ADMIN"> 
            <AdminPage />
          </RoleCheckRoute>
          } />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
