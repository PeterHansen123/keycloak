import keycloak from "../keycloak/keycloak"

export const TokenPage = () => {
    const tokenString = keycloak.token
    return (
        <>
            <h2>Token Page</h2>
            <pre>{tokenString}</pre>
        </>
    )
}