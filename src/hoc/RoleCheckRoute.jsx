import keycloak from "../keycloak/keycloak";

export const RoleCheckRoute = ({ children, role }) => {
  if (!keycloak.authenticated) {
    return (
      <>
        <h4>You are not auth to see this</h4>
      </>
    )
  }
  if (keycloak.hasRealmRole(role)) {
    return (<>{children}</>);
  } else {
    return (
        <>
            <h4>You lack sufficient auth to see this</h4>
        </>
    )
 
  }
};
