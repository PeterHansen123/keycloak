import keycloak from "../keycloak/keycloak"

export const LoggedinRoute = ({ children }) => {
    if ( keycloak.authenticated) {
        return (
            <>{ children }</>
        ) 
    } else {
        return (
            <>
                <h4>You are not auth to see this</h4>
            </>
        )
    }
}